package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        boolean isPlaying = true;
        while (isPlaying) {
            System.out.println("Let's play round " + roundCounter);
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase(Locale.ROOT);

            while (!rpsChoices.contains(humanChoice)) {
                System.out.println("I do not understand " + humanChoice + ". Could you try again?");
                humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase(Locale.ROOT);
            }

            String computerChoice = getComputerMove();
            System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + ". " + computeResult(humanChoice, computerChoice));
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            String continuePlaying = readInput("Do you wish to continue playing? (y/n)?");

            while (!continuePlaying.equals("y") && !continuePlaying.equals("n")) {
                System.out.println("I do not understand " + continuePlaying + ". Could you try again?");
                continuePlaying = readInput("Do you wish to continue playing? (y/n)?");
            }

            roundCounter += 1;
            if (continuePlaying.equals("n")) {
                isPlaying = false;
                System.out.println("Bye bye :)");
            }
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        return sc.next();
    }

    private String computeResult(String humanChoice, String computerChoice) {
        if (humanChoice.equals(computerChoice)) {
            return "It's a tie!";
        }
        switch (humanChoice) {
            case "rock":
                if (computerChoice.equals("scissors")) {
                    humanScore += 1;
                    return "Human wins!";
                }
                else {
                    computerScore += 1;
                    return "Computer wins!";
                }
            case "paper":
                if (computerChoice.equals("rock")) {
                    humanScore += 1;
                    return "Human wins!";
                }
                else {
                    computerScore += 1;
                    return "Computer wins!";
                }
            case "scissors":
                if (computerChoice.equals("paper")) {
                    humanScore += 1;
                    return "Human wins!";
                }
                else {
                    computerScore += 1;
                    return "Computer wins!";
                }
        }
        return "Unable to compute results.";
    }

    private String getComputerMove() {
        int rand = (int) (Math.random() * 3);
        return rpsChoices.get(rand);
    }
}
